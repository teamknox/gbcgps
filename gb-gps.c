/* ---------------------------------------- */
/*             GB-GBS Ver1.0                */
/*                  by                      */
/*               TeamKNOx                   */
/* ---------------------------------------- */

#include <gb.h>
#include "gb-gps.h"
#include "gb232.h"	

/* BackGround */
#include "gps_bg.c"

// Gameboy Color palette 0 (Normal condition)
#define CGBPal0c0 32767
#define CGBPal0c1 24311
#define CGBPal0c2 15855
#define CGBPal0c3 0

#define CGBPal1c1 31    // RED
#define CGBPal2c1 992   // GREEN
#define CGBPal3c1 31744 // BLUE
#define CGBPal4c1 31775 // GREEN + RED
#define CGBPal5c1 32736 // GREEN + BLUE
#define CGBPal6c1 1023  // BLUE + RED


UWORD pBkgPalette[] = {
	CGBPal0c0, CGBPal0c1, CGBPal0c2, CGBPal0c3
};


UWORD pObjPalette[] = {
	CGBPal0c0, CGBPal0c1, CGBPal0c2, CGBPal0c3, // Palette0
	CGBPal0c0, CGBPal1c1, CGBPal0c2, CGBPal0c3, // Palette1
	CGBPal0c0, CGBPal2c1, CGBPal0c2, CGBPal0c3, // Palette2
	CGBPal0c0, CGBPal3c1, CGBPal0c2, CGBPal0c3, // Palette3
	CGBPal0c0, CGBPal4c1, CGBPal0c2, CGBPal0c3, // Palette4
	CGBPal0c0, CGBPal5c1, CGBPal0c2, CGBPal0c3, // Palette5
	CGBPal0c0, CGBPal6c1, CGBPal0c2, CGBPal0c3  // Palette6
};

// object
#include "satelite.c"

// Display 
#define LEVEL_METER_BG		51
#define LEVEL_METER_FULL	59
#define LEVEL_METER_HALF	55


#define LED_IMAGE_0			60
#define LED_IMAGE_1			62
#define LED_IMAGE_DOT		82
#define LED_IMAGE_MINUS		92
#define LED_IMAGE_PLUS		94


char gWorkStr[WORK_LEN];				// for Display buffer
unsigned char gGpsData[DATA_SIZE + 1];	// GPS data receive buffer
UBYTE gGpsMode;							// GPS display mode
unsigned char gConfirmStr[CONFIRM_SIZE + 1] = {  // Confirmation strings
		'S', 'O', 'N', 'Y', CR, LF
	 };
unsigned char gSoftReset[] = { SOFTRESET };	    // Software reset
unsigned char gDatum_B[] = { DATUM_B };		      // Datum setting for Tokyo
unsigned char gCrlf[] = { CR, LF, 0 };

UBYTE flickkerfree;
UBYTE gErrorResult;


// DENCHI Mark
UWORD gGlobalCounter;               // Counter for general use
UWORD gGlobalCounterWork;           //
char gGlobalCounterStr[8]; //
UBYTE gDenchiMode;
extern UWORD nvm_gGlobalCounter;    //

extern UBYTE gRcvData[RECEIVE_SIZE];	

// RealOrSim
// test pattern from file
#include "test01.dat"

/* BG image */
unsigned char gbs1_tile[] = {
/*	 0,   1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 */
	  0,  1,  2,  3,  4,  5,  6,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
	  7,  8,  0,  0,  0,  0,  9, 10,  0,  0,  0, 96, 97, 98, 96, 99,100,  0,  0,  0,
	 11,  0, 29, 30, 31, 32,  0, 12,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
	 13,  0, 33,  0,  0, 34,  0, 14,  0, 92, 92, 92, 92,105, 92, 92,116, 92, 92, 92,
	 15,  0, 35,  0,  0, 36,  0, 16,  0, 93, 93, 93, 93,  0, 93, 93,  0, 93, 93, 93,
	 17,  0, 37, 38, 39, 40,  0, 18,  0, 92, 92, 92, 92,105, 92, 92,116, 92, 92, 92,
	 19, 20,  0,  0,  0,  0, 21, 22,  0, 93, 93, 93, 93,  0, 93, 93,  0, 93, 93, 93,
	  0, 23, 24, 25, 26, 27, 28,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
	  0, 92, 92, 80, 92, 92, 80, 92, 92,  0,  0, 92, 92, 80, 92, 92, 80, 92, 92,  0,
	  0, 93, 93, 81, 93, 93, 81, 93, 93,  0,  0, 93, 93, 81, 93, 93, 81, 93, 93,  0,
	  0, 41, 59, 59, 59, 59, 59, 59, 59, 59,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
	  0, 42, 59, 59, 59, 59, 59, 59, 59, 59,  0,  0,  0, 92, 92, 92,  0,  0,  0,  0,
	  0, 43, 59, 59, 59, 59, 59, 59, 59, 59,  0,  0,  0, 93, 93, 93,103,104,102,101,
	  0, 44, 59, 59, 59, 59, 59, 59, 59, 59,  0,  0, 92, 92, 92, 92,  0,  0,  0,  0,
	  0, 45, 59, 59, 59, 59, 59, 59, 59, 59,  0,  0, 93, 93, 93, 93,104,  0,  0,  0,
	  0, 46, 59, 59, 59, 59, 59, 59, 59, 59,  0,  0,  0, 92, 92, 92,105,  0,  0,  0,
	  0, 47, 59, 59, 59, 59, 59, 59, 59, 59,  0,  0,  0, 93, 93, 93,  0,  0,106,108,
	  0, 48, 59, 59, 59, 59, 59, 59, 59, 59,  0,  0,  0,  0,  0,  0,  0,  0,107,109
};

unsigned char tClearLevelMeter[] = {LEVEL_METER_BG};
unsigned char tPutLevelMeter[] = {LEVEL_METER_FULL};
unsigned char tHalfLevelMeter[] = {LEVEL_METER_HALF};

unsigned char tBatteryMark[] = {120, 122, 121, 123};

void send_data( unsigned char *data )	
{					
  while ( *data ) {			
	send_gb232( *data );		
	data++;				
  }					
}					

// GB
// GPS へ初期化コマンドを送る
UBYTE init_gps()
{
    send_data(gSoftReset);	    // Software reset
	send_data(gCrlf);
	send_data(gDatum_B);	    // Datum setting for Tokyo
	send_data(gCrlf);
	
	return 1;
}


// RealOrSim
UBYTE receive_gb232sim()
{
	UBYTE i;
	for(i = 0;i < 110;i++){
		gRcvData[i] = gTempBuff[i];
	}
	return 0;
}


// GB
/*
	配列の一定領域をコピーするプログラム
	
	注意
		エラーチェックは行っていないので source, dist の範囲に注意すること
*/
UBYTE blkcopy(UBYTE source_start, UBYTE dest_start, UBYTE length)
{
	UBYTE i;
	
	for(i = 0;i < length; i++){
		gWorkStr[i + source_start] = gRcvData[i + dest_start];
	}
	gWorkStr[i + source_start] = NULL;
	
	return i + source_start;
}


// GB
/*
	確認のための文字列の比較
	受信データの中の固定領域（常に一定のデータが出力される）の数値を
	比較することにより正しく受信できたかどうかのチェックを行う
	この方法は他のソフトウェアでも行われている（らしい）。
*/
UBYTE confirm_comm()
{
	UBYTE i;
	
	
	/* "SONY" の文字列が受信したバッファに格納されているかどうか？ */
	for(i = 0;i < 3;i++){
		if(gRcvData[i] != gConfirmStr[i]){
			return COM_ERROR;
		}
	}

	return NO_ERROR;

}


// Display 7seg led
//
UBYTE disp_lcd(UBYTE digit_no, UBYTE pos_x, UBYTE pos_y, char str[10])
{
	UBYTE i;
	unsigned char disp_tile[20];

	// Dimmed pattern making
	for(i = 0;i < digit_no;i++){
		disp_tile[i] = LED_IMAGE_0 + 12 * 2;
		disp_tile[i + digit_no] = LED_IMAGE_0 + 12 * 2 + 1;
	}
	set_bkg_tiles(pos_x, pos_y, digit_no, 2, disp_tile);


	// Display 7seg LED image display
	i=0;
	while(str[i]){
		if(str[i] >= '0'||'9' <= str[i]){
			disp_tile[i] = LED_IMAGE_0 + (str[i] - '0') * 2;
			disp_tile[i + digit_no] = LED_IMAGE_0 + (str[i] - '0') * 2 + 1;
		}
		switch(str[i]){
			case ':':
				disp_tile[i] = LED_IMAGE_0 + 10 * 2;
				disp_tile[i + digit_no] = LED_IMAGE_0 + 10 * 2 + 1;
			break;
			case '.':
				disp_tile[i] = LED_IMAGE_0 + 11 * 2;
				disp_tile[i + digit_no] = LED_IMAGE_0 + 11 * 2 + 1;
			break;

			case ' ':
				disp_tile[i] = LED_IMAGE_0 + 12 * 2;
				disp_tile[i + digit_no] = LED_IMAGE_0 + 12 * 2 + 1;
			break;

			case 'E':
			case 'e':
				disp_tile[i] = LED_IMAGE_0 + 13 * 2;
				disp_tile[i + digit_no] = LED_IMAGE_0 + 13 * 2 + 1;
			break;

			case 'N':
			case 'n':
				disp_tile[i] = LED_IMAGE_0 + 14 * 2;
				disp_tile[i + digit_no] = LED_IMAGE_0 + 14 * 2 + 1;
			break;

			case 'W':
			case 'w':
				disp_tile[i] = LED_IMAGE_0 + 15 * 2;
				disp_tile[i + digit_no] = LED_IMAGE_0 + 15 * 2 + 1;
			break;

			case '-':
				disp_tile[i] = LED_IMAGE_0 + 16 * 2;
				disp_tile[i + digit_no] = LED_IMAGE_0 + 16 * 2 + 1;
			break;

			case '+':
				disp_tile[i] = LED_IMAGE_0 + 17 * 2;
				disp_tile[i + digit_no] = LED_IMAGE_0 + 17 * 2 + 1;
			break;

			default:
			break;
		}
		i++;
	}
	set_bkg_tiles(pos_x, pos_y, digit_no, 2, disp_tile);


	return i;
}



// GB
UBYTE sat_level(UBYTE no_sat, UBYTE level)
{
	UBYTE i, m, n;
	
	m = level / 2;
	n = level % 2;

	// Put Level Meter
	for(i = 0;i < m;i++){
		 set_bkg_tiles(i + SAT_LEVEL_START_X, no_sat + SAT_LEVEL_START_Y, 1, 1, tPutLevelMeter);
	}

	// Put Half Level
	if(n)
		set_bkg_tiles(m + SAT_LEVEL_START_X, no_sat + SAT_LEVEL_START_Y, 1, 1, tHalfLevelMeter);

	// Clear Level Meter until MAX
	for(i = m + 1;i < SAT_LEVEL_MAX;i++){
		 set_bkg_tiles(i + SAT_LEVEL_START_X, no_sat + SAT_LEVEL_START_Y, 1, 1, tClearLevelMeter);
	}

	return i;
}

void sat_position(UBYTE pos_x, UBYTE pos_y, UBYTE sat_id, UBYTE sat_status)
{

	enable_interrupts();

    set_sprite_prop(sat_id, sat_status - 'A');
	move_sprite(sat_id, pos_x + 4, pos_y + 4);
}

// GB
UBYTE disp_error(UBYTE error_code)
{
	if(!flickkerfree){
		switch(error_code){
			case TIME_OUT_ERROR:
				disp_lcd(8, 1, 8, "00000001");
			break;
			case COM_ERROR:
				disp_lcd(8, 1, 8, "00000002");
			break;
			default:
			break;
		}
	}

	flickkerfree = 1;

	return NO_ERROR;
}

void dispDenchi(UWORD sec)
{
    UBYTE f, j, n;
    UWORD m;

    f = 0;m = 10000;
    for(j = 0;j < 5;j++){
        n = sec / m;
        sec = sec % m;
        m = m / 10;
        if((n == 0) && (f == 0)){
	        gGlobalCounterStr[j] = '0';
        }
        else{
	        f = 1;
	        gGlobalCounterStr[j] = n + '0';
        }
    }
}

// GB
/*
	IPS-5x00 のデータ出力フォーマット
	IPS-5x00 は GPS からの受信データを１秒間隔で 110Byte(CR, LF 含む)送信する。
	データの内容は以下のとおり
*/
int disp_gps_info1()
{
	UBYTE f, m, j, n, i, work1, work2, work3, work4, work5;
	unsigned char number[2];


    switch(gDenchiMode){
        case DENCHI_NORMAL:
	        // 測位の確認
	        // IPS-5x00 では、２０バイト目からのデータに緯度情報を
	        // あらわすデータ群が格納されるが、その先頭が "n" に
	        // なっていると未測位をあらわす
	        if(gRcvData[N_POSITION] == 'n'){
		        // 未測位
    	        // 演算時間	gps_receive[ 47] + 13
		        strcpy(gWorkStr, "--:--:--");
    	        disp_lcd(8, 11, 8, gWorkStr);
	        }
	        else{
		        // 測位が正しく行われた場合
    	        //	演算時間	gps_receive[ 47] + 13
	            blkcopy(0, 54, 2);	// xx:**:**
	            gWorkStr[ 2] = ':';	// **:*****
	            blkcopy(3, 56, 2);	// xx:xx***
	            gWorkStr[ 5] = ':';	// xx:xx:**
	            blkcopy(6, 58, 2);	// xx:xx:xx
	            disp_lcd(8, 11, 8, gWorkStr);
	        }
	    break;
	    case DENCHI_PAUSE:
            dispDenchi(gGlobalCounterWork);
	        disp_lcd(5, 14, 8, gGlobalCounterStr);
        break;
        case DENCHI_CONT:
        case DENCHI_CLEAR:
            dispDenchi(gGlobalCounter);
	        disp_lcd(5, 14, 8, gGlobalCounterStr);
        break;
        default:
        break;
    }

	
	//	緯度		gps_receive[ 19] +  8
	blkcopy(0, 19, 1);
	disp_lcd(1, 9, 3, gWorkStr);	// 北緯・南緯の表示

	disp_lcd(1, 10, 3, " ");		// 北緯・南緯は±９０°まで

	blkcopy(0, 20, 2);				// 「度」文字列コピー
	disp_lcd(2, 11, 3, gWorkStr);	// 「度」数値表示
									// 「度」記号表示

	blkcopy(0, 22, 2);				// 「分」文字列コピー
	disp_lcd(2, 14, 3, gWorkStr);	// 「分」数値表示
									// 「分」記号表示

	blkcopy(0, 24, 3);				// 「秒」文字列コピー
	disp_lcd(3, 17, 3, gWorkStr);	// 「秒」数値表示


	//	経度		gps_receive[ 27] +  9
	blkcopy(0, 27, 4);				// 「度」文字列コピー
	disp_lcd(4, 9, 5, gWorkStr);    // 「度」数値表示
                   					// 「度」記号表示

	blkcopy(0, 31, 2);				// 「分」文字列コピー
	disp_lcd(2, 14, 5, gWorkStr);   // 「分」数値表示
                   					// 「分」記号表示

	blkcopy(0, 33, 3);				// 「秒」文字列コピー
	disp_lcd(3, 17, 5, gWorkStr);   // 「秒」数値表示


    // GPS から来る文字列をデコードしてフォーマットを整える
    // GB 画面上に展開するデータは表示ブロック毎に処理していく

    //	計測時間	gps_receive[  6] + 13
    blkcopy(0, 13, 2);	// xx:**:**
    gWorkStr[ 2] = ':';	// **:*****
    blkcopy(3, 15, 2);	// xx:xx***
    gWorkStr[ 5] = ':';	// xx:xx:**
    blkcopy(6, 17, 2);	// xx:xx:xx
    disp_lcd(8, 1, 8, gWorkStr);

    /*	高度		gps_receive[ 36] +  5	*/
    blkcopy(0, 36, 4);
    disp_lcd(4, 12, 13, gWorkStr);

    /*	速度		gps_receive[ 41] +  3	*/
    blkcopy(0, 41, 3);
    disp_lcd(3, 13, 11, gWorkStr);

    /*	進行方向	gps_receive[ 44] +  3	*/
    blkcopy(0, 44, 3);
    disp_lcd(3, 13, 15, gWorkStr);

    /*	衛星位置 */
    /*	受信 GPS 衛星の仰角と方位角を求める */
    /*
	    仰角
	    仰角は +0°〜+90°の範囲を A 〜 Jに、-0°〜-90°の範囲をそれぞれ、
	    a 〜 jに分けている。A(a)〜J(j) の１０段階の位置をマッピングする。
	    GB のディスプレイの衛星の表示位置は X = 0 〜 63,Y = 0 〜 63 としている。
	    プラス位置とマイナス位置があるので、±５段階に分割可能であるが、GB の
    	表示エリアは 仰角 64dot 表示可能であるので、±で２分割、つまり±３２で
	    表示する。衛星位置は 32 / 10 = 3.2 であるが、計算が煩雑になるので最小
	    設定位置を４にする。最大値は 10 なので、補正係数は 4 x 10 = 40 となる。
	    （プログラムリスト参照）
	    表示位置が多少ずれるが大まかな位置がわかれば良いのでこの仕様とする。
	
	    方位角
    	同様に方位角は A(a) 〜 R(r) の 18 段階の範囲で値を取る。±３２の範囲に
	    近づけるには 18 x 2 = 36 つまり、最小分解能を２ドットとし、表示範囲を
	    ３６とすれば良い。

    */

    for(i = 0;i < NUMBER_SAT;i++){
	    work1 = gRcvData[64 + i * 5];			// 仰角
	    if(work1 >= 'A' && work1 <= 'J'){
		    work2 = 40 - (work1 - 'A') * 4;
	    }
	    if(work1 >= 'a' && work1 <= 'j'){
		    work2 = (work1 - 'a') * 4 + 40;
	    }

    	work1 = gRcvData[65 + i * 5];			// 方位角
	    if(work1 >= 'A' && work1 <= 'R'){
		    work3 = 36 - (work1 - 'A') * 2;
	    }
	    if(work1 >= 'a' && work1 <= 'r'){
		    work3 = (work1 - 'a') * 2 + 36;
	    }

	    work4 = gRcvData[63 + i * 5];			// 衛星の番号
	    if(work4 >= 'A' && work4 <= 'X'){
		    work5 = work4 - 'A';
	    }
	    if(work4 >= 'a' && work4 <= 'h'){
		    work5 = work4 - 'a' +  25;
	    }
	    
	    if(!(gGlobalCounter % FLICKER_RATE)){
	        //Display Satellite Obj 
	        sat_position(work2, work3, work5, gRcvData[66 + i * 5]);	// 衛星キャラクタの表示

            // Display Satellite Number
	        work5++;
	        f = 0;m = 10;
	        for(j = 0;j < 2;j++){
		        n = work5 / m;
		        work5 = work5 % m;
		        m = m / 10;
		        if((n == 0) && (f == 0)){
			        number[j] = 41;
		        }
		        else{
			        f = 1;
			        number[j] = n + 41;
		        }
		        set_bkg_tiles(0,10+i, 2, 10+i, number);
	        }
	    }

    }

    /* 衛星信号強度 */
    /*
	    衛星の信号強度は A〜Z まで定義されており、Z に近づくほど
	    信号強度は高くなる、信号強度 D から実際に測位が可能である
    */
        for(i = 0;i < NUMBER_SAT ;i++){
	        switch(gRcvData[67 + i * 5]){
		        case 'A':
			        work1 = 0;
			    break;

		        case 'B':
			        work1 = 1;
			    break;

		        case 'C':
			        work1 = 2;
			    break;

		        case 'D':
			        work1 = 3;
			    break;

		        case 'E':
		        case 'F':
			        work1 = 4;
			    break;

		        case 'G':
		        case 'H':
			        work1 = 5;
			    break;

		        case 'I':
		        case 'J':
			        work1 = 6;
			    break;

		        case 'K':
		        case 'L':
			        work1 = 7;
			    break;

		        case 'M':
		        case 'N':
			        work1 = 8;
			    break;

		        case 'O':
		        case 'P':
			        work1 = 9;
			    break;

		        case 'Q':
		        case 'R':
			        work1 = 10;
			    break;

		        case 'S':
		        case 'T':
			        work1 = 11;
			    break;

		        case 'U':
		        case 'V':
			        work1 = 12;
		    	break;

		        case 'W':
		        case 'X':
			        work1 = 13;
			    break;

		        case 'Y':
			        work1 = 14;
			    break;

		        case 'Z':
			        work1 = 15;
			    break;

		        default:
			        work1 = 0;
	        }
	    sat_level(i, work1);
    }

    return 1;

}


void init_bkg()
{
	// Initialize the background
	set_bkg_data( 0,  127, gbs1);
	set_bkg_tiles(0, 0, 20, 18, gbs1_tile);
	set_bkg_palette( 0, 1, pBkgPalette );

}

void init_sprite()
{
	UBYTE i;

	set_sprite_data(0, 31, satelite);
	
	for(i = 0;i < 32;i++){
		set_sprite_tile(i, i);
		set_sprite_prop( i, 0 );
	}
	set_sprite_palette( 0, 6, pObjPalette );

}


void main()
{

	UBYTE i;

	// GB initialize
	disable_interrupts();
	
	init_bkg();
	init_sprite();

	SHOW_BKG;
	SHOW_SPRITES;
	DISPLAY_ON;
	enable_interrupts();

    ENABLE_RAM_MBC1;
    SWITCH_RAM_MBC1(0);

    // Fix-up pad status at start-up
    strcpy(gGlobalCounterStr, "00000");
    disp_lcd(8, 11, 8, gWorkStr);
    gGlobalCounter = nvm_gGlobalCounter;
    if(joypad() & J_START){
        if(joypad() & J_SELECT){
            // Power ON with START + SELECT
            // START = 1, SELECT = 1
            gDenchiMode = DENCHI_CLEAR;
            nvm_gGlobalCounter = gGlobalCounter = gGlobalCounterWork = CLEAR;
            // Set Battery Mark
			set_bkg_tiles(18, 16, 2, 2, tBatteryMark);
        }
        else{
            // Power ON with START
            // START = 1, SELECT = 0
            gDenchiMode = DENCHI_CONT;
            gGlobalCounter = nvm_gGlobalCounter;
            // Set Battery Mark
			set_bkg_tiles(18, 16, 2, 2, tBatteryMark);
        }
    }
    else{
        if(joypad() & J_SELECT){
            // Power ON with SELECT
            // START = 0, SELECT = 1
            gDenchiMode = DENCHI_PAUSE;
            gGlobalCounterWork = nvm_gGlobalCounter;    // Store temporary
            // Set Battery Mark
			set_bkg_tiles(18, 16, 2, 2, tBatteryMark);
        }
        else{
            // Normal Power ON
            // START = 0, SELECT = 0
            gDenchiMode = DENCHI_NORMAL;
            // Set Normal condition Mark
        }
    }

    set_gb232( SPEED_9600 | DATA_8 | STOP_1 | PARITY_NONE );
    init_gb232();

    // Peripheral Initialize
    init_gps();                          // Initialize commnad to GPS receiver

    while(1){

//RealOrSim
//		delay(1000);					// Sim

		// Sync to GPS receiver
        // Receive GPS data
//RealOrSim
		do{							
			gErrorResult = receive_gb232( DATA_SIZE, 1 );			// Real
//			gErrorResult = receive_gb232sim();			// Sim
		}while(gErrorResult);

    	if(!confirm_comm()){
			disp_gps_info1();               // display GPS information
    	}
    	else{
			disp_error(COM_ERROR);      	// error handling
        	gErrorResult = CLEAR;			// Error Reset
    	}
        nvm_gGlobalCounter = gGlobalCounter;
        gGlobalCounter++;   // 
    }
}
