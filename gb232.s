;**********************************
; Serial Driver for Game Boy Color
; Apr21/2001
; TeamKNOx
;**********************************
;
;[Pin Assign. of GBC Serial Port]
;pin1 5V
;pin2 Sout = TxD
;pin3 nc
;pin4 P14  = RxD
;pin5 nc
;pin6 GND  = GND
;

	.area	_BSS
_gRcvData::
	.DS 127
.param_gb232:
	.DS 1

	.area	_CODE
_set_gb232::
	LDA	HL,2(SP)	;skip return address
	LD	A,(HL)
	LD	(.param_gb232),A
	RET

;------------------------------------------------------------------------------
_init_gb232::
	LD	B,#<127		;clear buffer
	LD	HL,#_gRcvData
	XOR	A
ini_1:	LD	(HL+),A
	DEC	B
	JR	NZ,ini_1

	LD	A,#0xFF		;send stop bit
	LDH	(#0x01),A
	LD	A,#0x83
	LDH	(#0x02),A
	RET

;------------------------------------------------------------------------------
_receive_gb232::
	DI
	PUSH	BC
	LDA	HL,4(SP)	;skip return address and registers
	LD	E,(HL)		; E = data length
	INC	HL
	LD	D,(HL)		; D = time-out counter
	PUSH	DE		;put time-out counter & data length
	LD	HL,#_gRcvData	; HL = store address
	PUSH	HL		;put store address

receive_start:
	LDH	A,(#0x56)					;12
	BIT	4,A						; 8
	JR	NZ,wait_for_start_bit				;12
	POP	HL		;get store address
	LD	(HL),#0x00	;clear data
	POP	DE
	LD	E,#0x02		;overrun error!
	POP	BC
	EI
	RET

wait_for_start_bit:
	CALL	receive_check			;28(return)	;24+28(call)
	LD	A,D				; 4
	CP	#<0				; 8
	JR	NZ,wait_for_data_bit		;12
	POP	HL		;get store address
	LD	(HL),#0x00	;clear data
	POP	DE
	LD	E,#0x01		;time-out error!
	POP	BC
	EI
	RET

wait_for_data_bit:
	LD	D,#0x00		;clear data	; 8
	LD	L,#0x00	;clear parity counter	; 8
	LD	E,#<8	;set bit counter	; 8
	LD	A,(.param_gb232)		;16
	BIT	3,A				; 8
	JR	Z,receive_next_bit		;12  8
	DEC	E		;data bit 7	;    4
	PUSH	BC		;NOP*4		;16		;16
	POP	BC		;NOP*3		;12		;12
	PUSH	BC		;NOP*4		;16		;16
	POP	BC		;NOP*3		;12		;12
	PUSH	BC		;NOP*4		;16		;16
	POP	BC		;NOP*3		;12		;12
	PUSH	BC		;NOP*4		;16		;16
	POP	BC		;NOP*3		;12		;12
	PUSH	BC		;NOP*4		;16		;16
	POP	BC		;NOP*3		;12		;12

receive_next_bit:
	CALL	delay				;24+(delay)	;24+(delay)
	PUSH	BC		;NOP*4		;16		;16
	POP	BC		;NOP*3		;12		;12
	NOP					; 4		; 4
	LDH	A,(#0x56)			;12		;12
						;=320+(delay)(shift timing 200clock)
	SWAP	A					; 8	; 8	; 8
	LD	B,A					; 4	; 4	; 4
	RRA			;put pin 4 into carry	; 4	; 4	; 4
	RR	D		;store data bit		; 8	; 8	; 8
	LD	A,B					; 4	; 4	; 4
	XOR	L		;count parity 		; 4	; 4	; 4
	LD	L,A					; 4	; 4	; 4
	DEC	E		;count bit		; 4	; 4	; 4
	JR	NZ,receive_next_bit			; 8	;12	; 8
								;=120+(delay)
wait_fot_parity_bit:
	LD	A,(.param_gb232)			;16		;16
	BIT	1,A					; 8		; 8
	JR	Z,wait_for_stop_bit2			; 8		;12

	LD	H,A					; 4
	CALL	delay					;24+(delay)
	LDH	A,(#0x56)				;12
							;=120+(delay)
	SWAP	A				; 4
	XOR	H				; 4
	XOR	L				; 4
	AND	#0x01				; 8
	JR	Z,wait_for_stop_bit		;12
	POP	HL		;get store address
	LD	(HL),#0x00	;clear data
	POP	DE
	LD	E,#0x04		;parity error!
	POP	BC
	EI
	RET

wait_for_stop_bit:
	PUSH	BC		;NOP*4		;16
	POP	BC		;NOP*3		;12
	NOP					; 4
	NOP					; 4
	NOP					; 4
	NOP					; 4
	NOP					; 4
	NOP					; 4
wait_for_stop_bit2:
	CALL	delay				;24+(delay)		;24+(delay)
	LDH	A,(#0x56)			;12			;12
						;=120+(delay)		;=120+(delay)
	BIT	4,A						; 8
	JR	NZ,data_store					;12
	POP	HL		;get store address
	LD	(HL),#0x00	;clear data
	POP	DE
	LD	E,#0x03		;framing error!
	POP	BC
	EI
	RET

data_store:
	LD	A,(.param_gb232)				;16
	BIT	3,A						; 8
	JR	Z,str_1						;12  8
	SRL	D						;    8
str_1:	POP	HL		;get store address		;12
	LD	(HL),D		;store data			; 8
	INC	HL		;next store address		; 8
	POP	DE	;get time-out counter & data length	;12
	DEC	E		;count data length		; 4
	PUSH	DE		;put data length		;16
	PUSH	HL		;put store address		;16
	JP	NZ,receive_start				;16
								;=232(include receive_start)
	POP	HL
	POP	DE
	LD	E,#0x00		;received
	POP	BC
	EI
	RET

;------------------------------------------------------------------------------
receive_check:
				;time-out time = D * 0.1sec
				;D = time-out counter
				;0.1sec (4.194304MHz*0.1sec = 419430clock)
				;->(44*232+20)*41+20=419368clock
	LD	B,#<232							; 8
	LD	C,#<41							; 8
chk_loop:
	LDH	A,(#0x56)			;12			;12
	BIT	4,A				; 8		; 8	;=28(call)
	RET	Z		;received	; 8		;20
	DEC	B				; 4		;=28(return)
	JR	NZ,chk_loop			;12	-4
	LD	B,#<232				;=44	 8
	DEC	C				;	 4
	JR	NZ,chk_loop			;	12	-4
	LD	C,#<41				;	=20	 8
	DEC	D				;		 4
	JR	NZ,chk_loop			;		12
	RET			;time-out!	;		=20

;------------------------------------------------------------------------------
_send_gb232::
	DI
	PUSH	BC
	LDA	HL,4(SP)	;skip return address and registers
	LD	D,(HL)		; D = send data
set_parity:
	LD	B,D
	LD	E,#<8		;set data counter
	LD	A,(.param_gb232)
	BIT	3,A
	JR	Z,par_1
	DEC	E		;data bit 7
par_1:	LD	C,E
	LD	L,#<0		;clear parity counter
par_2:	LD	A,B
	RRCA
	LD	B,A
	XOR	L
	LD	L,A
	DEC	E
	JR	NZ,par_2

	LD	A,(.param_gb232)
	LD	H,A
	XOR	L
	AND	#0x01
	LD	L,A		;set parity
	LD	E,C		;set data counter
send_start_bit:
	LD	A,#0x00
	LDH	(#0x01),A
	LD	A,#0x83
	LDH	(#0x02),A
send_next_bit:
	CALL	delay				;24+(delay)
	NOP					; 4
	NOP					; 4
	NOP					; 4
	NOP					; 4
	NOP					; 4
	NOP					; 4
	XOR	A		;A = 0x00	; 4
	RR	D				; 8
	JR	NC,snd_1			;12  8
	CPL			;A = 0xFF	;    4
snd_1:	LDH	(#0x01),A			;12
	LD	A,#0x83				; 8
	LDH	(#0x02),A			;12
	DEC	E		;count bit	; 4		; 4		; 4
	JR	NZ,send_next_bit		;12		; 8		; 8
						;=120+(delay)
send_parity_bit:
	CALL	delay						;24+(delay)	;24+(delay)
	NOP							; 4		; 4
	NOP							; 4		; 4
	NOP							; 4		; 4
	BIT	1,H						; 8		; 8
	JR	Z,send_stop_bit					; 8		;12

	XOR	A		;A = 0x00			; 4
	BIT	0,L						; 8
	JR	Z,sen_2						;12  8
	CPL			;A = 0xFF			;    4
sen_2:	LDH	(#0x01),A					;12
	LD	A,#0x83						; 8
	LDH	(#0x02),A					;12
								;=120+(delay)
	CALL	delay					;24+(delay)
	PUSH	BC		;NOP*4			;16
	POP	BC		;NOP*3			;12
	NOP						; 4
	NOP						; 4
	NOP						; 4
	NOP						; 4
send_stop_bit:
	NOP						; 4			; 4
	NOP						; 4			; 4
	NOP						; 4			; 4
	LD	A,#0xFF					; 8			; 8
	LDH	(#0x01),A				;12			;12
	LD	A,#0x83					; 8			; 8
	LDH	(#0x02),A				;12			;12
							;=120+(delay)		;=120+(delay)
	CALL	delay						;24+(delay)
	POP	BC
	EI
	RET

;------------------------------------------------------------------------------
delay:
	LD	A,(.param_gb232)		;16
	AND	#0xF0				; 8
	CP	#0x70		;14400bps	; 8
	JR	NZ,dly_1			; 8	;12
	LD	BC,#3				;12
	NOP			;min		; 4
;	NOP			;max		;
	JR	delay_loop			;12
dly_1:
	CP	#0x60		;9600bps		; 8
	JR	NZ,dly_2				; 8	;12
	LD	BC,#7					;12
	NOP						; 4
	NOP						; 4
	NOP						; 4
	NOP						; 4
	NOP			;min			; 4
	NOP						; 4
	NOP						; 4
;	NOP						;
;	NOP						;
;	NOP			;max			;
	JR	delay_loop				;12
dly_2:
	CP	#0x50		;4800bps			; 8
	JR	NZ,dly_3					; 8	;12
;	LD	BC,#23		;min				;
	LD	BC,#24						;12
;	LD	BC,#25		;max				;
	JR	delay_loop					;12
dly_3:
	CP	#0x40		;2400bps				; 8
	JR	NZ,dly_4						; 8	;12
;	LD	BC,#53		;min					;
	LD	BC,#55							;12
;	LD	BC,#58		;max					;
	JR	delay_loop						;12
dly_4:
	CP	#0x30		;1200bps					; 8
	JR	NZ,dly_5					;12		; 8
;	LD	BC,#115		;min						;
	LD	BC,#121								;12
;	LD	BC,#127		;max						;
	JR	delay_loop							;12
dly_5:
	CP	#0x20		;600bps				; 8
	JR	NZ,dly_6				;12	; 8
;	LD	BC,#239		;min				;
	LD	BC,#252						;12
;	LD	BC,#265		;max				;
	JR	delay_loop					;12
dly_6:
	CP	#0x10		;300bps			; 8
	JR	NZ,dly_7			;12	; 8
;	LD	BC,#487		;min			;
	LD	BC,#514					;12
;	LD	BC,#541		;max			;
	JR	delay_loop				;12
dly_7:
	CP	#0x00		;110bps		; 8
	JR	NZ,dly_8			; 8
;	LD	BC,#1347	;min		;
	LD	BC,#1422			;12
;	LD	BC,#1497	;max		;
	JR	delay_loop			;12
dly_8:
	LD	BC,#0xFFFF	;speed param. error
	RET

delay_loop:
	DEC	BC			; 8
	LD	A,B			; 4
	OR	C			; 4
	JR	NZ,delay_loop		;12	-4
	RET				;	16
					;=28*BC+12
;[clock calc.]
;4194304Hz/14400bps=  291clock
;4194304Hz/ 9600bps=  437clock
;4194304Hz/ 4800bps=  874clock
;4194304Hz/ 2400bps= 1748clock
;4194304Hz/ 1200bps= 3495clock
;4194304Hz/  600bps= 6991clock
;4194304Hz/  300bps=13981clock
;4194304Hz/  110bps=38130clock

;End of gb232.s