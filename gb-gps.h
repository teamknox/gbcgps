/*
	gb-gps.h
			1999.01.02	
			2001.03.10	Modify
			2001.03.17	Modify
*/

#define	CONFIRM			108						// Data length from GPS-receiver except CR/LF
#define	OFFSET_CONFIRM	4						// Lenght of "SONY" is 4.
#define	DATA_SIZE		110						// Data length from GPS-receiver
#define RECEIVE_SIZE	127						// Data buffer size

#define	CONFIRM_SIZE	6						// Confirmation string size
#define	CLEAR			0						// Initialize
#define	CR				0x0d
#define	LF				0x0a
#define	N_POSITION		19						// Confirmation char position
#define	WORK_LEN		20						// Work Area size
#define NUMBER_SAT      8                       // Number of Satelite
#define SOFTRESET       "@SR"                   // GPS Software reset
#define	DATUM_B			"@SKB"					// Datum is TOKYO

#define NO_ERROR		0
#define TIME_OUT_ERROR	1
#define COM_ERROR   	2

#define SAT_LEVEL_START_X   2
#define SAT_LEVEL_START_Y   10
#define SAT_LEVEL_STEP_Y    1
#define SAT_LEVEL_MAX		8

#define FLICKER_RATE    4

#define DENCHI_NORMAL   0
#define DENCHI_PAUSE    1
#define DENCHI_CONT     2
#define DENCHI_CLEAR    3

