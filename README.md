# GPS receiver handling by Gameboy color
GB-GPS is driver and application software for connection between GBC and SONY IPS-5x00 series. Serial driver is developed by Mr.K.I of TeamKNOx. UI + Serial driver is GB-GPS. 

## How it works
UI detail is [here](./pics/GB-GPS.pdf).
<table>
<tr>
<td><img src="./pics/gb-gps01.jpg"></td>
<td><img src="./pics/gb-gps02.jpg"></td>
<td><img src="./pics/gb-gps03.jpg"></td>
</tr>
</table>
IPS-5x00 sends 110byte data packet in one second in periodically. GBC reads the data packet, and formatting. Display shows as similar as Germin receiver. GB-GPS will be created by your own idea. At moment, not sophisticated, however, we will tune as soon as possible.Right Picture is latest version of GB-GPS. I found a small problem which is flicker in updated at one seconds. Probably, update rate will be 4 second in each. I think this working(tuning..) is professinal working. (Otherhand only selfish :-<)Satellite color will be changed by satellites status. Identify is very easy... 

## Cable
You have to make connector cable for using GB-GPS. TeamKNOx has been considering [To make generic serial cable](./pics/Gb232_cbl.pdf). Therefore, cost reduction will be realized. We have to use all connection cable at GBC side. Genuine Nitendo is not all connection cable. GameTech is supporting all connection cable. You can obtain by mail order. Your friend will be happy, if you made two set of cable and give spare one to him/her. 

## Connections
<table>
<tr>
<td><img src="./pics/gb-gps_iz1.jpg"></td>
<td><img src="./pics/gb-gps_iz2.jpg"></td>
<td><img src="./pics/gb-gps_iz3.jpg"></td>
</tr>
</table>
Pictures show conneciton GBC and GPS-receiver. We confirmed GPS-receiver which can be connected IPS-5000, IPS-5100G, IPS-5200. Also pictures show equipment as GAbattery which is connected in bottom side of GBC. GB-GPS got practical using by GAbattery. For mountaining GPS-reciver is realized by GBC light-magic. Finally, I got it and used it... 

## How to develop
At 1st, we made simualtor of GPS receiver. We can separate to develop driver and UI part by the simualtor. Sound of Simulator is quite complex. However, very simple architecture, we put on acutal data to array of C program. You can connect to PC, if you checkted correct working on simualtor. PC connection testing is quite tough, receive robustness testing by PC connection. 

## Applications
GB-GPS is basic driver and application. You can add your own function and application. I have been considering to develop UI part of GB-GPS. In some time, I encountered a problem which lost satelite in display. Display updated in one second in periodcally. I'd like to solve by using with more sophisticated tecniq. For example to use histrisys in display routine. I am wondering that how to handle in other programs. 

### DENCH mark test
<table>
<tr>
<td><img src="./pics/gb-gps04.jpg"></td>
</tr>
</table>
In generally, performance testing is called "BENCH Mark Test". Bench is testing table... Battery life testing can be excuted by GB-GPS. Dench means Battery in Japanese, so Dench and Bench are closed pronounciation each other ? GPS receiver gives a signal packet in every one second, which is connected to GB-GPS. Counting the signal and store to NVRAM until battery life, therefore we can measure battery life... 

## Thanks to
In development GB-GPS, We have to thank to Mr.Takao SHIMIZU who is GPS forum of niftyserve. He gave me a lot data and advise. 

# License
Copyright (c) Osamu OHASHI  
Distributed under the MIT License either version 1.0 or any later version. 
