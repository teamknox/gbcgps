CC	= /usr/local/sbin/GBDK-2.0/bin/lcc -Wa-l -Wl-m

BINS	= gb-gps.gb

all:	$(BINS)

%.o:	%.c
	$(CC) -c -o $@ $<

%.o:	%.s
	$(CC) -c -o $@ $<

%.gb:	%.o
	$(CC) -o $@ $<

clean:
	rm -f *.o *.lst *.map *.gb

